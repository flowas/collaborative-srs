<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="imp_standard.xsl" />

<xsl:template match="project">

<html>

  <head>

    <title><xsl:value-of select="title" /></title>

    <link rel="stylesheet" type="text/css" href="css/doc.css" />
    <link rel="stylesheet" type="text/css" href="css/menu.css" />

  </head>

  <body>

    <xsl:call-template name="main-menu" />
   
  <div id="doc">
    <center>

      <h2><xsl:value-of select="title" /></h2>

      Version: <xsl:value-of select="version" />

    </center>

    <xsl:apply-templates select="dl" />


    </div>
  </body>

</html>

</xsl:template>

<xsl:template match="dl">
  <dl>
    <xsl:apply-templates select="dt|dd" />
  </dl>
</xsl:template>

<xsl:template match="dt">
  <dt><xsl:value-of select="." /></dt>
</xsl:template>

<xsl:template match="dd">
  <dd><xsl:value-of select="." /></dd>
</xsl:template>

<xsl:template match="nfrs">

</xsl:template>

<xsl:template match="useCaseTree">

</xsl:template>

</xsl:stylesheet>

