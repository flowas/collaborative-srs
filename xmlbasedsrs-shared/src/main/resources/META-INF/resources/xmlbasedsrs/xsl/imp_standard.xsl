<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


<xsl:template match="dl">
  <dl>
    <xsl:apply-templates select="dt|dd" />
  </dl>
</xsl:template>

<xsl:template match="dt">
  <dt><xsl:value-of select="." /></dt>
</xsl:template>

<xsl:template match="dd">
  <dd><xsl:apply-templates /></dd>
</xsl:template>


<xsl:template match="img">
  <xsl:choose>

  <xsl:when test="@class='imgText'">
  <br clear="all" />
  <br />
  <center>
  <div>

  <img>
    <xsl:attribute name="src">
      <xsl:value-of select="@src" />
    </xsl:attribute>
    <xsl:attribute name="align">
      <xsl:value-of select="@align" />
    </xsl:attribute>
    <xsl:attribute name="alt">
      <xsl:value-of select="." />
    </xsl:attribute>
    <xsl:attribute name="class">
      <xsl:value-of select="@class" />
    </xsl:attribute>
  </img>

  <br clear="all" />

  <span class="imgText"><xsl:number count="img" format="1"/>. <xsl:value-of select="." /></span>

  </div>
  </center>
  <br clear="all" />
  </xsl:when>
  <xsl:otherwise>

  <img>
    <xsl:attribute name="src">
      <xsl:value-of select="@src" />
    </xsl:attribute>
    <xsl:attribute name="align">
      <xsl:value-of select="@align" />
    </xsl:attribute>
    <xsl:attribute name="alt">
      <xsl:value-of select="@alt" />
    </xsl:attribute>
    <xsl:attribute name="class">
      <xsl:value-of select="@class" />
    </xsl:attribute>
  </img>
  </xsl:otherwise>
  </xsl:choose>
</xsl:template>


<xsl:template match="p">
  <p>
    <xsl:apply-templates />
  </p>
</xsl:template>

<xsl:template match="br">
  <br />
</xsl:template>



<xsl:template name="main-menu">

  <div id="main-menu" class="menu">

   <div style="float:right;background-color:transparent;border:0px;">
     <a href="http://xmlbasedsrs.tigris.org">xmlbasedsrs.tigris.org</a>
   </div>

   <form name="docView">
     <select onchange="location=this.options[this.selectedIndex].value;">
       <option value="">--- Select view ---</option>
       <option value="index.html">Introduction</option>
       <option value="nfr.html">Non functional requirements</option>
       <option value="tree.html">Tree view of Use Cases</option>
       <option value="demo.html">Demonstration</option>
       <option value="map.html">Map of requirement dependencies</option>
       <option value="xmldiff.html">Differences between versions</option>
       <option value="print.html">Printable</option>
       <option value="project.mrproject">MrProject</option>
       <option value="list.html">Work view</option>
     </select>
  
   </form>

  </div>

</xsl:template>

</xsl:stylesheet>