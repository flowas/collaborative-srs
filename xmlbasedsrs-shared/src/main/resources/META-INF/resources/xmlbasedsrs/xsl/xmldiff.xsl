<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="imp_standard.xsl" />

<xsl:template match="files">

  <html>

  <head>

    <link rel="stylesheet" type="text/css" href="css/xmldiff.css" />

    <style type="text/css">
      #modifiedCol {
        display: none;
      }
    </style>

    <style type="text/css">
      .modified {
        background-color: #d2d2d2;
      }
    </style>

    <link rel="stylesheet" type="text/css" href="css/doc.css" />
    <link rel="stylesheet" type="text/css" href="css/menu.css" />
    <link rel="stylesheet" type="text/css" href="css/tooltip.css" />

    <script type="text/javascript" language="Javascript" src="js/tools.js"></script>
    <script type="text/javascript" language="Javascript" src="js/domTT.js"></script>
    <script>
      var domTT_prefix = 'domTTOverlib';
    </script>

  </head>

  <body>

    <xsl:call-template name="main-menu" />

    <div id="sub-menu" class="menu">

      <div>
        <a href="javascript:initiateToggle(1);" title="Show/Hide status line">
         Status</a>

      </div>

      <div>
        <a href="javascript:initiateToggle(2);" title="Show/Hide changed requirements">
        Changes</a>

      </div>

    

    </div>

    <div id="doc">

      <h2>Differences between versions</h2>

   

      <p>Different versions of the Software Requirements Specifications are compared to track changes.</p>

      <xsl:call-template name="file">
     
        <xsl:with-param name="preDoc" select="1" />

        <xsl:with-param name="doc1" select="2" />

        <xsl:with-param name="lastDoc" select="count(//file)" />

        <xsl:with-param name="filesToDiff" select="/files/file[1]" />

      </xsl:call-template>


    </div>



  </body>

  </html>

</xsl:template>


<xsl:template name="file">

  <xsl:param name="preDoc" />

  <xsl:param name="doc1" />

  <xsl:param name="lastDoc" />

  <xsl:param name="filesToDiff" />
     

        <table cellpadding="0" cellspacing="0" align="left">

          <tr>

            <th><xsl:value-of select="document(/files/file[$doc1])//project/version" /></th>

          </tr>     


          <xsl:for-each select="document(//file[$doc1])//requirement">

            <xsl:sort select="number(translate(@id, 'R', ''))" data-type="number" order="ascending" />

            <xsl:variable name="rId" select="@id" />   

            <xsl:variable name="rDescription" select="description" />                  

            

            <tr>          

              <td>                

                  <xsl:attribute name="class">status<xsl:value-of select="@status" /></xsl:attribute>


                  <div>                   
                
                    <xsl:if test="$doc1>1">
                  
                   

                    <xsl:if test="document(document($filesToDiff)/files/file[$preDoc])//requirement[attribute::id=$rId and description != $rDescription] ">       

                      <xsl:attribute name="class">modified</xsl:attribute>

<xsl:attribute name="onmouseover">return domTT_true(domTT_activate(this, event, 'status', ''));</xsl:attribute>
   <xsl:attribute name="onmousemove">domTT_activate(this, event, 'caption', '<xsl:value-of select="normalize-space(description)" />', 'content', '<xsl:value-of select="normalize-space(document(document($filesToDiff)/files/file[$preDoc])//requirement[attribute::id=$rId]/description)" /> ');</xsl:attribute>


                    </xsl:if>

                    </xsl:if>

                    <a href="" class="req">
   
                      <xsl:attribute name="onmouseover">return domTT_true(domTT_activate(this, event, 'status', ''));</xsl:attribute>

                      <xsl:attribute name="onmousemove">domTT_activate(this, event, 'content', '<xsl:value-of select="normalize-space(description)" /> ');</xsl:attribute>

                      <xsl:value-of select="$rId" />

                    </a>

               
                  </div>                

                </td>

              </tr>

              <!-- 

                Add empty rows for deleted requirements

              -->        
              
              <xsl:call-template name="removedRequirement">

                <xsl:with-param name="numId" select="number(translate(@id, 'R', ''))" />

                <xsl:with-param name="lastId" select="count(document(document($filesToDiff)/files/file[$doc1])//requirement)" />

                <xsl:with-param name="doc1" select="document($filesToDiff)/files/file[$doc1]" />

         

              </xsl:call-template>

              

          </xsl:for-each>

           
        </table>  



    <xsl:if test="$doc1&lt;$lastDoc">

      <xsl:call-template name="file">
     
        <xsl:with-param name="preDoc" select="$doc1" />

        <xsl:with-param name="doc1" select="$doc1+1" />

        <xsl:with-param name="lastDoc" select="$lastDoc" />

        <xsl:with-param name="filesToDiff" select="$filesToDiff" />

      </xsl:call-template>

    </xsl:if>

</xsl:template>


<xsl:template match="requirement">
     
  <xsl:variable name="reqId" select="concat(@id, ' ')" />    

  <div>

    <xsl:attribute name="title"><xsl:value-of select="normalize-space(description)" /></xsl:attribute>
    <xsl:attribute name="class">status<xsl:value-of select="@status" /></xsl:attribute>

    <xsl:value-of select="@id" />

  </div>

</xsl:template>


<xsl:template name="removedRequirement">

  <xsl:param name="numId" />

  <xsl:param name="lastId" />

  <xsl:param name="doc1" />
        
 

    <xsl:if test="not(document($doc1)//requirement[attribute::id = concat( 'R' ,$numId + 1) ] )" >

      <xsl:if test="$numId&lt;$lastId">

      <tr><td><br /></td></tr>



        <xsl:call-template name="removedRequirement">

          <xsl:with-param name="numId" select="$numId+1" />

          <xsl:with-param name="lastId" select="$lastId" />

          <xsl:with-param name="doc1" select="$doc1" />

        </xsl:call-template>

      </xsl:if>


    </xsl:if>


</xsl:template>

</xsl:stylesheet>

