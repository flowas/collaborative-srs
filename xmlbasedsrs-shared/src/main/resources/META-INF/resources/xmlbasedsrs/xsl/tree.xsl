<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

 <xsl:import href="imp_standard.xsl" />
 <xsl:import href="imp_useCaseTree.xsl" />

<xsl:template match="useCaseTree">

  <html>
 
    <head>



      <link rel="stylesheet" type="text/css" href="css/tree.css" />
      <link rel="stylesheet" type="text/css" href="css/requirement.css" />


      <style type="text/css">

        div.requirement, div.reqRef {
          display: none;
        }

      </style>


      <link rel="stylesheet" type="text/css" href="css/treeInfo.css" />
      <link rel="stylesheet" type="text/css" href="css/doc.css" />
      <link rel="stylesheet" type="text/css" href="css/menu.css" />
      <link rel="stylesheet" type="text/css" href="css/useCase.css" />

      <script type="text/javascript" language="Javascript" src="js/tools.js">
      </script>

    </head>
    <body>

      <xsl:call-template name="main-menu" />

      <div id="sub-menu" class="menu">

      <div>
        <a href="javascript:initiateToggle(2);" title="Show/Hide requirements">
         Requirements</a>

      </div>

      <div>
        <a href="javascript:initiateToggle(3);">
        Info</a>

      </div>

      </div>

      <div id="doc">

      <div id="infoBox">

        <table>

          <tr>

            <td>

              Number of Use Cases:

            </td>
            <td>

              <xsl:value-of select="count(//useCase)" />

            </td>

          </tr>

          <tr>

            <td>

              Number of requirements:

            </td>
            <td>

              <xsl:value-of select="count(//requirement)" />

            </td>

          </tr>


          <tr>

            <td valign="top">

              Actors:

            </td>
            <td>

              <xsl:for-each select="//actor">

                <xsl:value-of select="@id" /> : <xsl:value-of select="name" /><br />

              </xsl:for-each>

            </td>

          </tr>


        </table>              

      </div>

        <h2>Tree view of use cases</h2>

        <xsl:apply-templates select="dl" />

        <menu>

          <xsl:for-each select="//useCase">
            <xsl:sort select="@priority" data-type="number" order="ascending" />
            
            <li>

              <a>

                <xsl:attribute name="href">#<xsl:value-of select="@id" /></xsl:attribute>

                <xsl:value-of select="@id" /> : 
                <xsl:call-template name="useCaseName" />

              </a>

            </li>

          </xsl:for-each>        

        </menu>

        <xsl:apply-templates select="useCase">
          <xsl:sort select="@priority" data-type="number" order="ascending" />
        </xsl:apply-templates>

      </div>

    </body>

  </html>

</xsl:template>

<xsl:template match="nfrs">

</xsl:template>

<xsl:template match="project">

</xsl:template>

</xsl:stylesheet>

