<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">



<xsl:import href="imp_useCaseTree.xsl"/>
<xsl:import href="imp_standard.xsl"/>
<xsl:import href="nfr.xsl" />

<xsl:template match="srs">

<html>

  <head>

    <title><xsl:value-of select="title" /></title>

    <link rel="stylesheet" type="text/css" href="css/doc.css" />
    <link rel="stylesheet" type="text/css" href="css/nfr.css" />
    <link rel="stylesheet" type="text/css" href="css/tree.css" />
    <link rel="stylesheet" type="text/css" href="css/useCase.css" />
  </head>

  <body id="doc">

    <xsl:apply-templates/>

  </body>

</html>

</xsl:template>

<xsl:template match="nfrs">
    <h2>Non functional requirements</h2>

    <xsl:apply-templates />	

</xsl:template>

<xsl:template match="project">
   
    <center>

      <h2><xsl:value-of select="title" /></h2>

      Version: <xsl:value-of select="version" />

    </center>

    <br />

    <span style="page-break-after:always" />

    <xsl:apply-templates select="dl" />

    <span style="page-break-after:always" />
</xsl:template>

<xsl:template match="useCaseTree">

        <h2>Tree view of use cases</h2>

 	<xsl:apply-templates select="dl" />

        <xsl:apply-templates select="useCase">
          <xsl:sort select="@priority" data-type="number" order="ascending" />
        </xsl:apply-templates>


</xsl:template>



</xsl:stylesheet>