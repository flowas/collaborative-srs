<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="imp_useCaseTree.xsl" />

<xsl:template match="nfrs">
  <html>
    <head>
      <link rel="stylesheet" type="text/css" href="css/doc.css" />
      <link rel="stylesheet" type="text/css" href="css/requirement.css" />
      <link rel="stylesheet" type="text/css" href="css/requirementHidden.css" />
      <link rel="stylesheet" type="text/css" href="css/menu.css" />
      <link rel="stylesheet" type="text/css" href="css/useCase.css" />
      <script type="text/javascript" language="Javascript" src="js/tools.js">
      </script>

    </head>
  <body>

  <xsl:call-template name="main-menu" />

  <div id="sub-menu" class="menu">

    <div>
        <a href="javascript:initiateToggle(2);">
        Requirements</a>

    </div>

  </div>
  <div id="doc">

    <xsl:apply-templates />	
 
  </div>

  </body>

</html>

</xsl:template>


<xsl:template match="useCaseTree">

</xsl:template>

<xsl:template match="project">

</xsl:template>

<xsl:template match="description">
  <xsl:apply-templates />
</xsl:template>




</xsl:stylesheet>
