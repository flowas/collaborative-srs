<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="imp_standard.xsl" />

<xsl:template match="useCaseTree">

  <html>

  <head>

    <link rel="stylesheet" type="text/css" href="css/doc.css" />
    <link rel="stylesheet" type="text/css" href="css/menu.css" />
    <link rel="stylesheet" type="text/css" href="css/list.css" />
    <link rel="stylesheet" type="text/css" href="css/listNew.css" />
    <link rel="stylesheet" type="text/css" href="css/listReviewed.css" />
    <link rel="stylesheet" type="text/css" href="css/listValidated.css" />

    <link rel="stylesheet" type="text/css" href="css/listUseCase.css" />



    <script type="text/javascript" language="Javascript" src="js/tools.js">
    </script>


  </head>
  <body>

      <xsl:call-template name="main-menu" />

      <div id="sub-menu" class="menu">

      <div>
        <a href="javascript:initiateToggle(6);" class="menuA" title="Show/Hide use cases">
         Use Cases</a>

      </div>

      <div style="background-color:red;">
        <a href="javascript:initiateToggle(3);" class="menuA" title="Show/Hide new requirements">
         New</a>

      </div>

      <div style="background-color:yellow;">
        <a href="javascript:initiateToggle(4);" class="menuA" title="Show/Hide reviewed requirements">
         Reviewed</a>

      </div>

      <div  style="background-color:#76d03e;">
        <a href="javascript:initiateToggle(5);" class="menuA" title="Show/Hide validated requirements">
         Validated</a>

      </div>

      </div>


  <div id="doc">



  <table>

    <tr>

    <td valign="top">

    <div id="New">

      <xsl:call-template name="reqHeader" />

      <xsl:apply-templates select="//requirement[@status='New']">
            <xsl:sort select="number(translate(@id, 'R', ''))" data-type="number" order="ascending" />
      </xsl:apply-templates>

    </div>

    </td>
    <td valign="top">

    <div id="Rev">

      <xsl:call-template name="reqHeader" />

      <xsl:apply-templates select="//requirement[@status='Rev']">
            <xsl:sort select="number(translate(@id, 'R', ''))" data-type="number" order="ascending" />
      </xsl:apply-templates>

    </div>

    </td>
    <td valign="top">

    <div id="Val">

      <xsl:call-template name="reqHeader" />

      <xsl:apply-templates select="//requirement[@status='Val']">
            <xsl:sort select="number(translate(@id, 'R', ''))" data-type="number" order="ascending" />
      </xsl:apply-templates>

    </div>

    </td>
    </tr>

  </table>

  </div>

  </body>

  </html>

</xsl:template>

<xsl:template name="reqHeader">

  <table>

    <tr>

      <th>

        <xsl:value-of select="status" />     

      </th>

    </tr>

      <tr>                 

        <th valign="top" width="50" align="center">

          Id

        </th>

        <th valign="top" width="300">

	  Krav

        </th>


      </tr>

    </table>

</xsl:template>


<xsl:template match="requirement">

  <xsl:variable name="reqId" select="concat(@id, ' ')" />


      <div class="requirement">

         <xsl:attribute name="id"><xsl:value-of select="@id" /></xsl:attribute>

         <div style="float:right; padding:0px 5px 0px 5px; border: 1px solid #d2d2d2;margin-right:3px;">

            <a style="text-decoration:none;">

              <xsl:attribute name="href">javascript:shDiv('<xsl:value-of select="@id"/>')</xsl:attribute>
                x
            </a>

         </div>

      <table>

      <tr>


        <td valign="top" width="50" align="center">

          <xsl:value-of select="@id" />

        </td>

        <td valign="top" width="300">

	  <xsl:value-of select="description" />

        </td>

       </tr>

       
       <tr>

         <td></td>
         <td>

          <xsl:call-template name="useCase" />

         <xsl:for-each select="//reqRef">

           <xsl:if test="contains(@ids,$reqId)">


            <xsl:call-template name="useCase" />
  
    
           </xsl:if>

         </xsl:for-each>
  

         </td>

       </tr>

       </table>

       </div>




</xsl:template>




<xsl:template name="useCase">
  
  <xsl:variable name="uId" select="ancestor::useCase/@id" />  

  <!--

    The title of each useCase is gets an achor <a name="id">title</a>

  -->

  <div class="useCase">

    <div class="useCaseTopBox">

    <span class="useCaseID">

    <xsl:value-of select="$uId" />

    </span>

    :

    <span class="actors">

      <xsl:variable name="actors" select="ancestor::useCase/@actors" />

      <xsl:for-each select="//actor">

        <xsl:variable name="a-id" select="concat(@id, ' ')" />

        <xsl:if test="contains($actors,$a-id)">

          <xsl:value-of select="name" />    

          <xsl:if test="string-length($a-id)=string-length(substring-after($actors,$a-id))">

            och

          </xsl:if>

          <xsl:if test="string-length(substring-after($actors,$a-id))>=string-length($a-id)*2">
   
            ,
    
          </xsl:if>
                
        </xsl:if>

      </xsl:for-each>
    
    </span>

    <span class="useCasetitle">

    <a>

      <xsl:attribute name="name">

        <xsl:value-of select="$uId" />

      </xsl:attribute>

    </a>

    &#160;<xsl:value-of select="ancestor::useCase/title" />

    </span>

    <div class="description">

      <xsl:value-of select="ancestor::useCase/description" />

    </div>


    </div>

  </div>  

</xsl:template>

<xsl:template match="nfrs">

</xsl:template>

<xsl:template match="project">

</xsl:template>

</xsl:stylesheet>

