<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes"/>

<xsl:template match="srs">

  <project company="" manager="" project-start="" mrproject-version="2">

    <xsl:attribute name="name">
      <xsl:value-of select="project/title" />
    </xsl:attribute>

    <tasks>

      <xsl:apply-templates select="useCaseTree" />

    </tasks>

    <resource-groups />

    <resources />

    <allocations />

  </project>

</xsl:template>


<xsl:template match="useCaseTree">

  <xsl:apply-templates select="useCase">

    <xsl:sort select="@priority" />

  </xsl:apply-templates>

</xsl:template>

<xsl:template match="useCase">
  
  <task>
   
    <xsl:attribute name="id">

      <xsl:number format="1" value="position()" from="/" />

    </xsl:attribute>  

    <xsl:attribute name="name"><xsl:value-of select="@id" /> : <xsl:value-of select="title" /></xsl:attribute>  

    <xsl:attribute name="note">      

    </xsl:attribute>  

    <xsl:attribute name="work">115000</xsl:attribute>  

    <xsl:attribute name="start">

    </xsl:attribute>  

    <xsl:attribute name="end">

    </xsl:attribute>  

    <xsl:attribute name="percente-complete">0</xsl:attribute>  

  </task>
  
</xsl:template>




</xsl:stylesheet>