<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="imp_standard.xsl" />

<xsl:template match="useCaseTree">

  <html>

  <head>

    <link rel="stylesheet" type="text/css" href="css/doc.css" />
    <link rel="stylesheet" type="text/css" href="css/menu.css" />
    <link rel="stylesheet" type="text/css" href="css/list.css" />
    <link rel="stylesheet" type="text/css" href="css/listUseCase.css" />

    <script type="text/javascript" language="Javascript" src="js/tools.js">
    </script>


  </head>
  <body onLoad="initiateToggle(3);">

    
  <xsl:call-template name="main-menu" />

  <div id="doc">

  <h2>Map of requirement dependencies</h2>

  <p>

   <xsl:for-each select="//requirement">
      <xsl:sort select="number(translate(@id, 'R', ''))" data-type="number" order="ascending" />
      <a>
       
        <xsl:attribute name="href">#<xsl:value-of select="@id" /></xsl:attribute>

        <xsl:value-of select="@id" />

      </a>, 
  
    </xsl:for-each> 

  </p>

<div>

      <xsl:call-template name="reqHeader" />

      <xsl:apply-templates select="//requirement" >

        <xsl:sort select="number(translate(@id, 'R', ''))" data-type="number" order="ascending" />

      </xsl:apply-templates>


    </div>
  



  </div>

  </body>

  </html>

</xsl:template>

<xsl:template name="reqHeader">

  <table>

    <tr>

      <th>

        <xsl:value-of select="status" />     

      </th>

    </tr>

      <tr>                 

        <th valign="top" width="50" align="center">

          Id

        </th>

        <th valign="top" width="600">

	  Requirement

        </th>


      </tr>

    </table>

</xsl:template>


<xsl:template match="requirement">

  <xsl:variable name="reqId" select="concat(@id, ' ')" />


      <div class="requirement">

      <table>

      <tr>


        <td valign="top" width="50" align="center">

          <a><xsl:attribute name="name"><xsl:value-of select="@id" /></xsl:attribute></a>

          <xsl:value-of select="@id" />

        </td>

        <td valign="top" width="600">

	  <xsl:value-of select="description" />

        </td>

       </tr>

       
       <tr>

         <td></td>
         <td>

          <xsl:call-template name="useCase" />

         <xsl:for-each select="//reqRef">

           <xsl:if test="contains(@ids,$reqId)">


            <xsl:call-template name="useCase" />
  
    
           </xsl:if>

         </xsl:for-each>
  

         </td>

       </tr>

       </table>

       </div>




</xsl:template>




<xsl:template name="useCase">
  
  <xsl:variable name="uId" select="ancestor::useCase/@id" />  

  <!--

    The title of each useCase is gets an achor <a name="id">title</a>

  -->

  <div class="useCase">

    <div class="useCaseTopBox">

    <span class="useCaseID">

    <xsl:value-of select="$uId" />

    </span>

    :

    <span class="actors">

      <xsl:variable name="actors" select="ancestor::useCase/@actors" />

      <xsl:for-each select="//actor">

        <xsl:variable name="a-id" select="concat(@id, ' ')" />

        <xsl:if test="contains($actors,$a-id)">

          <xsl:value-of select="name" />    

          <xsl:if test="string-length($a-id)=string-length(substring-after($actors,$a-id))">

            och

          </xsl:if>

          <xsl:if test="string-length(substring-after($actors,$a-id))>=string-length($a-id)*2">
   
            ,
    
          </xsl:if>
                
        </xsl:if>

      </xsl:for-each>
    
    </span>

    <span class="useCasetitle">

    <a>

      <xsl:attribute name="name">

        <xsl:value-of select="$uId" />

      </xsl:attribute>

    </a>

    &#160;<xsl:value-of select="ancestor::useCase/title" />

    </span>

    <div class="description">

      <xsl:value-of select="ancestor::useCase/description" />

    </div>

    Dependencies: 

    <xsl:for-each select="ancestor::useCase/descendant::requirement">
      
      <a>
       
        <xsl:attribute name="href">#<xsl:value-of select="@id" /></xsl:attribute>

        <xsl:value-of select="@id" />

      </a>, 
  
    </xsl:for-each> 

    </div>


  </div>  

</xsl:template>

<xsl:template match="nfrs">

</xsl:template>

<xsl:template match="project">

</xsl:template>

</xsl:stylesheet>

