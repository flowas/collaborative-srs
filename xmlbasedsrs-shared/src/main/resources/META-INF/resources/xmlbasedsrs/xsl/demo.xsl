<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="imp_standard.xsl" />
<xsl:import href="imp_useCaseTree.xsl" />

  <xsl:template match="useCaseTree">
    <html>

      <head>

        <link rel="stylesheet" type="text/css" href="css/doc.css" />
        <link rel="stylesheet" type="text/css" href="css/menu.css" />
        <link rel="stylesheet" type="text/css" href="css/demo.css" />
        <link rel="stylesheet" type="text/css" href="css/useCase.css" />

        <script type="text/javascript" language="Javascript" src="js/tools.js">
        </script>

      </head>

      <body>

      <xsl:call-template name="main-menu" />

      <div id="sub-menu" class="menu">

        <div id="formBox">

              <form>

                <select onChange="shUseCase(this.options[this.selectedIndex].value)">

                  <option value="info">--- Select Use Case ---</option>

                  <xsl:for-each select="useCase">
  
                    <option>

                      <xsl:attribute name="value"><xsl:value-of select="@id" />
                      </xsl:attribute>
                

                      <xsl:value-of select="@id" />

                      :

                      <xsl:call-template name="useCaseName" />


                    </option>

                  </xsl:for-each>

                </select>

              </form>

        </div>

      </div>

      <div id="doc">

        <xsl:for-each select="useCase">

          <xsl:call-template name="useCase" />

        </xsl:for-each>

        <div id="info">
          

          V&#228;lj vilket anv&#228;ndarfall du vill demonstrera i fliken.

        </div>

      </div>

      </body>
    </html>
  </xsl:template>

  <xsl:template match="actor">
  <xsl:value-of select="name" />

  ,&#160;</xsl:template>

  <xsl:template name="useCase">


  <!--

    The title of each useCase is gets an achor <a name="id">title</a>

  -->

  <div class="useCase" style="display: none;">

      <xsl:attribute name="id">
        <xsl:value-of select="@id" />
      </xsl:attribute>


    <div class="useCaseTopBox">

    <span class="useCaseID">

    <xsl:value-of select="@id" />

    </span>

    :

    <span class="actors">

      <xsl:variable name="actors" select="@actors" />

      <xsl:for-each select="//actor">

        <xsl:variable name="a-id" select="concat(@id, ' ')" />

        <xsl:if test="contains($actors,$a-id)">

          <xsl:value-of select="name" />    

          <xsl:if test="string-length($a-id)=string-length(substring-after($actors,$a-id))">

            och

          </xsl:if>

          <xsl:if test="string-length(substring-after($actors,$a-id))>=string-length($a-id)*2">
   
            ,
    
          </xsl:if>
                
        </xsl:if>

      </xsl:for-each>
    
    </span>

    &#160;	

    <span class="useCasetitle">

    <a>

      <xsl:attribute name="name">

        <xsl:value-of select="@id" />

      </xsl:attribute>

    </a>

    <xsl:value-of select="title" />

    </span>

    <div class="description">

      <xsl:value-of select="description" />

    </div>




      </div>



      <br clear="all" />

      <br clear="all" />


      <table cellpadding="0" cellspacing="0">

        <tr>

          <td>

            <xsl:apply-templates select="action" />

          </td>

        </tr>

      </table>

    </div>

  </xsl:template>

  <xsl:template match="action|reaction">
    <table border="0" cellpadding="0" cellspacing="5">
      <tr>
        <td width="160">
          <xsl:choose>
            <xsl:when test="name()='action'">
              <xsl:attribute name="class">actionBox</xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="class">reactionBox</xsl:attribute>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:choose>
            <xsl:when test="child::action|child::reaction">
              <a>
                <xsl:attribute name="href">javascript:shDiv('<xsl:value-of select="generate-id(child::action|child::reaction)" />')</xsl:attribute>

                <xsl:value-of select="description" />
              </a>
            </xsl:when>

            <xsl:otherwise>
              <xsl:value-of select="description" />
            </xsl:otherwise>
          </xsl:choose>
        </td>

        <xsl:if test="child::action|child::reaction">
          <td class="actionArrow">
            <img src="img/action.jpg" align="middle" />
          </td>
        </xsl:if>

        <td>
          <div style="display: none;">
            <xsl:attribute name="id">
              <xsl:value-of
              select="generate-id(child::action|child::reaction)" />
            </xsl:attribute>

            <xsl:apply-templates
            select="child::action|child::reaction" />
          </div>
        </td>
      </tr>
    </table>
  </xsl:template>

 
 <xsl:template match="useCaseRef">
    <div class="useCaseRef">
      <xsl:variable name="ids" select="@ids" />
      <xsl:value-of select="//useCase[@id=$ids]/title" />
    </div>
  </xsl:template>


  <xsl:template match="requirement">
    <dl style="margin: 0px;">
      <dt>
        <table>
          <tr>
            <td valign="top">
            <a>
              <xsl:attribute name="name">
                <xsl:value-of select="@id" />
              </xsl:attribute>

              <xsl:value-of select="@id" />
            </a>

            :</td>

            <td valign="top">
              <xsl:value-of select="description" />
            </td>
          </tr>
        </table>
      </dt>

      <dd>
        <div style="page-break-inside: avoid;">
          <xsl:apply-templates select="child::requirement" />
        </div>
      </dd>
    </dl>
  </xsl:template>

<xsl:template match="nfrs">

</xsl:template>

<xsl:template match="project">

</xsl:template>

</xsl:stylesheet>

