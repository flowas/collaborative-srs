<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="imp_standard.xsl" />

<xsl:template name="useCaseName">

    <span class="actors">

      <xsl:variable name="actors" select="concat(@actors, ' ')" />

      <xsl:for-each select="//actor">

        <xsl:variable name="a-id" select="concat(@id, ' ')" />

        <xsl:if test="contains($actors,$a-id)">

          <xsl:value-of select="name" />    

          <xsl:if test="string-length($a-id)=string-length(substring-after($actors,$a-id))">

            och

          </xsl:if>

          <xsl:if test="string-length(substring-after($actors,$a-id))>=string-length($a-id)*2">
   
            ,
    
          </xsl:if>
                
        </xsl:if>

      </xsl:for-each>
    
    </span>

    <span class="useCasetitle">

    &#160;<xsl:value-of select="title" />

    </span>

</xsl:template>

<xsl:template match="requirement">

    <div class="requirement">

    <dl style="margin: 0px;">

      <dt>

         <table>
         
           <tr>

	     <td valign="top" style="white-space: nowrap;">   	      

              <xsl:value-of select="@id" />

              :

            </td>
            <td valign="top">

              <xsl:value-of select="description" />

            </td>

          </tr>

        </table>

      </dt>

      <dd>

        <div style="page-break-inside: avoid;">

          <xsl:apply-templates select="child::requirement|child::reqRef" />

        </div>

      </dd>

    </dl>

    </div>

</xsl:template>




<!--

  Requirements are shown in a indented dl list as they are written in the 
  srs.xml file.  

-->

<xsl:template name="requirement">

    <div class="requirement">
    
    <dl style="margin: 0px;">

      <dt>

         <table>
         
           <tr>

	     <td valign="top" style="white-space: nowrap;">

               <a>
   	       <xsl:attribute name="name">

                 <xsl:value-of select="@id" />

               </xsl:attribute> 

               <xsl:value-of select="@id" />

              </a> :

            </td>
            <td valign="top">

              <xsl:value-of select="description" />

            </td>

          </tr>

        </table>

      </dt>

      <dd>

        <div style="page-break-inside: avoid;">

          <!--

            Recursive calls to the requirement or reqRef template to show
            the child requirements.

          -->

          <xsl:apply-templates select="child::requirement|child::reqRef" />

        </div>

      </dd>

    </dl>

    </div>

</xsl:template>



<xsl:template match="actor">

  <xsl:value-of select="name" />,&#160;

</xsl:template>


<xsl:template match="action|reaction">

  <table border="0" cellpadding="0" cellspacing="0">

  <tr>

    <!-- 

      Apply the hierachy template to show the tree structure

    -->

    <xsl:call-template name="hierarchy" />

    <!--

      Show the action or reaction icon 

    -->
   
    <td valign="top">

      <xsl:attribute name="class">

        <xsl:if test="child::requirement|child::reqRef">

          <xsl:if test="child::action|child::reaction">

	    bar

          </xsl:if>

        </xsl:if>

      </xsl:attribute>

      <img>

        <xsl:attribute name="src">img/<xsl:value-of select="name()" />.jpg</xsl:attribute>

     </img>

    </td>



    <td valign="top">

      <div class="arDescription">

        <xsl:value-of select="description" />

      </div>

      <!--
 
        Show the requirements for this action/reaction

      -->

      <xsl:if test="requirement|reqRef">

        <xsl:apply-templates select="requirement|reqRef" />

      </xsl:if>

    </td>

  </tr>

  </table>

  <!--

    Apply next action/reaction 

  -->

  <xsl:apply-templates select="child::action|child::reaction" />

</xsl:template>


<xsl:template match="reqRef">

  <div class="reqRef">
            
      <xsl:variable name="req-ids" select="concat(@ids, ' ')" />    

      <xsl:for-each select="//requirement">

        <xsl:if test="contains($req-ids,concat(@id, ' '))">
          
          <xsl:call-template name="requirement" />
                
        </xsl:if>

      </xsl:for-each>
           
  </div>

</xsl:template>


<xsl:template match="useCase">
  
  <!--

    The title of each useCase is gets an achor <a name="id">title</a>

  -->

  <a>

    <xsl:attribute name="name">

      <xsl:value-of select="@id" />

    </xsl:attribute>

  </a>

  <div class="useCase">

    <div class="useCaseTopBox">  

    <span class="useCaseID">

    <xsl:value-of select="@id" />

    </span>

    :

    <span class="actors">

      <xsl:variable name="actors" select="concat(@actors, ' ')" />

      <xsl:for-each select="//actor">

        <xsl:variable name="a-id" select="concat(@id, ' ')" />

        <xsl:if test="contains($actors,$a-id)">

          <xsl:value-of select="name" />    

          <xsl:if test="string-length($a-id)=string-length(substring-after($actors,$a-id))">

            och

          </xsl:if>

          <xsl:if test="string-length(substring-after($actors,$a-id))>=string-length($a-id)*2">
   
            ,
    
          </xsl:if>
                
        </xsl:if>

      </xsl:for-each>
    
    </span>

    <span class="useCasetitle">

    <a>

      <xsl:attribute name="name">

        <xsl:value-of select="@id" />

      </xsl:attribute>

    </a>

    &#160;<xsl:value-of select="title" />

    </span>

    <div class="description">

      <xsl:apply-templates select="description" />

    </div>




    <xsl:if test="child::requirement|child::reqRef">

      <xsl:apply-templates select="requirement|reqRef" /> 

    </xsl:if>

    </div>

    <xsl:apply-templates select="action" /> 

  </div>  

</xsl:template>




<xsl:template name="hierarchy">
  <xsl:for-each select="ancestor::*[name()!='chapter' and name()!='useCase']">
    
    <xsl:choose>
      <xsl:when test="following-sibling::action|following-sibling::reaction">

	  <td class="bar">
  	    <img src="img/transbar.gif"/>
          </td>

      </xsl:when>
      <xsl:otherwise>

        <td>
          <img src="img/transbar.gif"/>
        </td>

      </xsl:otherwise>
    </xsl:choose>
  </xsl:for-each>
  

      <xsl:choose>
        <xsl:when test="following-sibling::action | following-sibling::reaction">
          <td class="tbar">
            <img src="img/tbar.gif"/>
          </td>
        </xsl:when>
        <xsl:otherwise>
          <td class="ebar">
            <img src="img/ebar.gif"/>
          </td>
        </xsl:otherwise>
      </xsl:choose>

</xsl:template>

</xsl:stylesheet>

