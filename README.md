#背景
本项目的灵感来自于一个叫 [xmlbasersrs](http://xmlbasedsrs.tigris.org/) 的项目，不过这个项目已经停止更新十多年了。

#软件需求规格说明书
SRS(Software Requirements Specification), 软件需求说明书的编制是为了使用户和软件开发者双方对该软件的初始规定有一个共同的理解， 使之成为整个开发工作的基础。包含硬件、功能、性能、输入输出、接口界面、警示信息、保密安全、数据与数据库、文档和法规的要求。

#特性设计
× 基于xml编写软件需求规格说明，xml满足xsd模式定义。
× 用xslt可以把xml转换为html。
× 在html中可以渲染CMMN（用例模型标记语言）标记。
× xml可以转换为符合testlink样式的xml，以便于导入textlink中编辑和使用。

#社区交流
QQ群：345359985