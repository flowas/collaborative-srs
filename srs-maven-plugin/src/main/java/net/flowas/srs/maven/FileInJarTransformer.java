package net.flowas.srs.maven;

import java.io.File;
import java.net.URL;
import java.util.Properties;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class FileInJarTransformer {
	static TransformerFactory tFactory = TransformerFactory.newInstance();
	public static String xml_xslt_html(File xmlFile, String xslFileName,
			File htmlFile) throws Exception {		
		URL url = Thread.currentThread().getContextClassLoader().getResource("META-INF/resources/xmlbasedsrs/xsl/"+xslFileName);
		StreamSource source = new StreamSource(url.openStream(), url.toExternalForm());
		Transformer tx = tFactory.newTransformer(source);

		Properties properties = tx.getOutputProperties();
		properties.setProperty(OutputKeys.ENCODING, "UTF-8");
		properties.setProperty(OutputKeys.METHOD, "html");
		tx.setOutputProperties(properties);

		StreamSource xmlSource = new StreamSource(xmlFile);
		StreamResult result = new StreamResult(htmlFile);

		tx.transform(xmlSource, result);

		return htmlFile.getAbsolutePath();
	}
}
