package net.flowas.srs.maven;

import java.io.File;
import java.util.Properties;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class XslMain {

	public static void main(String[] args) throws Exception {
		String xmlFileName = "src/main/resources/NewFile.xml";
		String xslFileName = "src/main/xmlbasedsrs/xsl/";
		String targetFileName = "target/";
        //demo
		xml_xslt_html(xmlFileName, xslFileName+"demo.xsl", targetFileName+"demo.html");
		xml_xslt_html(xmlFileName, xslFileName+"index.xsl", targetFileName+"index.html");
		xml_xslt_html(xmlFileName, xslFileName+"list.xsl", targetFileName+"list.html");
		xml_xslt_html(xmlFileName, xslFileName+"map.xsl", targetFileName+"map.html");
		xml_xslt_html(xmlFileName, xslFileName+"print.xsl", targetFileName+"print.html");
		xml_xslt_html(xmlFileName, xslFileName+"nfr.xsl", targetFileName+"nfr.html");
		xml_xslt_html(xmlFileName, xslFileName+"tree.xsl", targetFileName+"tree.html");
		xml_xslt_html(xmlFileName, xslFileName+"xmldiff.xsl", targetFileName+"xmldiff.html");
		xml_xslt_html(xmlFileName, xslFileName+"mrproject.xsl", targetFileName+"project.mrproject");
		xml_xslt_html(xmlFileName, xslFileName+"myPrint.xsl", targetFileName+"myPrint.html");
	}

	public static String xml_xslt_html(String xmlFileName, String xslFileName,
			String htmlFileName) throws Exception {
		TransformerFactory tFactory = TransformerFactory.newInstance();
		StreamSource source = new StreamSource(new File(xslFileName));
		Transformer tx = tFactory.newTransformer(source);

		Properties properties = tx.getOutputProperties();
		properties.setProperty(OutputKeys.ENCODING, "UTF-8");
		properties.setProperty(OutputKeys.METHOD, "html");
		tx.setOutputProperties(properties);

		StreamSource xmlSource = new StreamSource(new File(xmlFileName));
		File targetFile = new File(htmlFileName);
		StreamResult result = new StreamResult(targetFile);

		tx.transform(xmlSource, result);

		return targetFile.getAbsolutePath();
	}
}
