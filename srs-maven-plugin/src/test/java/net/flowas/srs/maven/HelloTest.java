package  net.flowas.srs.maven;

import java.io.File;

import net.flowas.srs.maven.GreetingMojo;

import org.apache.maven.plugin.MojoExecutionException;
import org.junit.Test;


public class HelloTest {
    @Test
	public void test() throws MojoExecutionException{
    	GreetingMojo mojo=new GreetingMojo();
    	mojo.setIncludes("*.xml");
    	mojo.setSourceDirectory(new File("src/xmlbasedsrs/"));
    	mojo.setTargetDirectory(new File("target/xmlbasedsrs/xhtml5"));
    	mojo.execute();
  }
}
